from django.test import TestCase

class UnitTest(TestCase):
    def test_feedback(self):
        response = self.client.get("/feedback/")
        self.assertEqual(response.status_code, 200)

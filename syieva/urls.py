from django.urls import path

from . import views

urlpatterns = [
    path('', views.feedback, name = 'feedback'),
    path('create/', views.feedback_create, name='feedback_create'),
    path('delete/<int:id>/', views.feedback_delete, name='feedback_delete'),
]
from django.shortcuts import render, redirect
from .models import Feedback
from . import forms
from django.http import HttpResponse


def feedback(request):
    feedbacks = Feedback.objects.all()
    return render(request, 'feedback.html', {'feedbacks' : feedbacks})

def feedback_create(request):
    if request.method == 'POST':
        form = forms.FeedbackForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('feedback')
    else:
        form = forms.FeedbackForm()
    return render(request, 'createFeedback.html', {'form': form})

def feedback_delete(request, id):
    if request.method == 'POST':
        Feedback.objects.filter(id=id).delete()
        return redirect('feedback')
    else:
        return HttpResponse("/GET not allowed")

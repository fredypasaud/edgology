from django.test import TestCase, LiveServerTestCase
from .models import Machine
from django.utils import timezone
from django.urls import reverse
from .forms import FormSignUpMesin
import datetime
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.common.action_chains import ActionChains

# Create your tests here.
class SignUpMesinTest(TestCase):

    def create_mesin(self, machine_name ="Betty", machine_type="Rotor", manufacturer="SamYoung", year_built=1990, location="Bojong Kasuok", status=1):
        return Machine.objects.create(machine_name=machine_name, machine_type=machine_type, manufacturer=manufacturer, year_built=year_built, location=location, status=status)

    def test_machine_create(self):
        temp = self.create_mesin()
        year = Machine.getTodayYear(temp)
        self.assertTrue(isinstance(temp, Machine))
        self.assertEqual(temp.__str__(), temp.machine_name)
        self.assertEqual(temp.getNamaMesin(), temp.machine_name)
        self.assertEqual(temp.getTipeMesin(), temp.machine_type)
        self.assertEqual(temp.getManufaktur(), temp.manufacturer)
        self.assertEqual(temp.getTahun(), temp.year_built)
        self.assertEqual(temp.getLokasi(), temp.location)
        self.assertEqual(temp.getStatus(), temp.status)
        self.assertGreaterEqual(year, temp.getTahun())
        temp.statusSafe()
        self.assertEqual(temp.getStatus(), 1)
        temp.statusRepair()
        self.assertEqual(temp.getStatus(), 2)
        temp.statusError()
        self.assertEqual(temp.getStatus(), 3)


    def test_overview(self):
        temp = self.create_mesin()
        url = reverse('machoverview')
        resp = self.client.get(url)

        self.assertEqual(resp.status_code, 200)

    def test_signUpMesin_FAIL(self):
        temp = FormSignUpMesin()
        url = reverse('signUpMesin')
        resp = self.client.get(url)

        self.assertEqual(resp.status_code, 302)

    def test_signupmachineform(self):
        temp = self.create_mesin()
        data = {'machine_name': temp.machine_name, 'machine_type':temp.machine_type, 'manufacturer':temp.manufacturer, 'year_built':temp.year_built, 'location':temp.location, 'status':temp.status}
        form = FormSignUpMesin(data=data)

        self.assertTrue(form.is_valid())

    def test_signupmachineform_invalid(self):
        temp = self.create_mesin()
        data = {'machine_type':temp.machine_type, 'manufacturer':temp.manufacturer, 'year_built':temp.year_built, 'location':temp.location, 'status':temp.status}
        form = FormSignUpMesin(data=data)
        self.assertFalse(form.is_valid())

    def test_eng_det(self):
        c = self.create_mesin()
        resp = self.client.get('/machine/overview/1')
        self.assertEqual(resp.status_code, 200)
    
    def test_index(self):
        temp = self.create_mesin()
        url = reverse('index')
        resp = self.client.get(url)

        self.assertEqual(resp.status_code, 302)
    
    def test_timeTravel(self):
        temp = self.create_mesin()
        url = reverse('timeTravel')
        resp = self.client.get(url)

        self.assertEqual(resp.status_code, 302)

    #def test_signupmachineformPOSTpass(self):
    #    url = reverse('signUpMesin')
    #    data = {'machine_name':"Rudy", 'machine_type':"Nyeh", 'manufacturer':"Starks", 'year_built':2002, 'location':"Bojong", 'status':1}
    #    resp = self.client.post(url, data)
    #    tempGet = Machine.objects.filter(machine_name=data['machine_name'])
    #    temp = tempGet.first()
    #    self.assertTrue(isinstance(temp, Machine))
    #    self.assertEqual(temp.__str__(), temp.machine_name)
    #    self.assertEqual(temp.getNamaMesin(), temp.machine_name)
    #    self.assertEqual(temp.getTipeMesin(), temp.machine_type)
    #    self.assertEqual(temp.getManufaktur(), temp.manufacturer)
    #    self.assertEqual(temp.getTahun(), temp.year_built)
    #    self.assertEqual(temp.getLokasi(), temp.location)
    #    self.assertEqual(temp.getStatus(), temp.status)
    #    self.assertEqual(resp.status_code, 302)

    def test_signupmachineformPOSTfail(self):
        url = reverse('signUpMesin')
        data = {'machine_name':"Rudy", 'manufacturer':"Starks", 'year_built':2002, 'location':"Bojong", 'status':1}
        resp = self.client.post(url, data)
        tempGet = Machine.objects.filter(machine_name=data['machine_name'])
        temp = tempGet.first()

        self.assertEqual(resp.status_code, 302)

class FunctionalTest(LiveServerTestCase):

    def setUp(self):
        options = Options()
        options.add_argument("--headless")
        self.selenium = webdriver.Firefox(firefox_options=options)
        super(FunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(FunctionalTest, self).tearDown()

from django.contrib import admin
from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('sign-up-mesin/', views.signUpMesin, name='signUpMesin'),
    path('overview/', views.overview, name='machoverview'),
    path('sign-up-mesin/goback/', views.timeTravel, name='timeTravel'),
    path('overview/<int:id>', views.see_engine, name = 'mach_det'),
    path('sign-up-mesin/getNamaMesin/', views.getNamaMesin, name='getNamaMesin')   
]

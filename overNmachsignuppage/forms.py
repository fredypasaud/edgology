from django import forms
from .models import Machine
import datetime
from django.core.validators import MaxValueValidator, MinValueValidator

class FormSignUpMesin(forms.ModelForm):
    class Meta:
        model = Machine
        fields = ('machine_name', 'machine_type', 'manufacturer', 'year_built', 'location')
    field_order = ['machine_name', 'machine_type', 'manufacturer', 'year_built', 'location']

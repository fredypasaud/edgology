from django.shortcuts import render, redirect
from django.http import HttpResponse, JsonResponse
from .forms import FormSignUpMesin
from .models import Machine
import datetime
from random import randint
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt

# Create your views here.

def index(request):
	return redirect('overview')

def overview(request):
	AllMachine = Machine.objects.all()
	ErrorMachines = [i for i in AllMachine if i.getStatus()==3]
	RepairingMachines = [i for i in AllMachine if i.getStatus()==2]
	SafeMachines = [i for i in AllMachine if i.getStatus()==1]
	ErrCount = len(ErrorMachines)
	RepCount = len(RepairingMachines)
	SafeCount = len(SafeMachines)
	return render(request,'overview.html', {"ErrorM":ErrorMachines, "RepairM":RepairingMachines, "SafeM":SafeMachines, "ErrC":ErrCount, "RepC":RepCount, "SafeC":SafeCount})

@login_required(login_url='/login/')
@csrf_exempt
def signUpMesin(request):
	if request.method == "GET":
		print("djksajdkasjd")
		forms = FormSignUpMesin()
		return render(request, 'signUpMesin.html', {"form":forms})
	elif request.method == "POST":
		b = datetime.date.today()
		form = FormSignUpMesin(request.POST)
		print("hdajshdajd")
		if form.is_valid():
			print("Hello")
			cleanMachineName = form.cleaned_data["machine_name"]
			cleanMachineType = form.cleaned_data["machine_type"]
			cleanManufacturer = form.cleaned_data["manufacturer"]
			cleanYearBuilt = form.cleaned_data["year_built"]
			cleanLocation = form.cleaned_data["location"]
			t = Machine(machine_name=cleanMachineName, machine_type=cleanMachineType, manufacturer=cleanManufacturer, year_built=cleanYearBuilt, location=cleanLocation, status=randint(1, 3))
			t.save()
			response = {
				'machineName' : cleanMachineName
			}
			return JsonResponse(response)
		return HttpResponse(status=400)

def timeTravel(request):
	return redirect('signUpMesin')

def see_engine(request,id):
	machine = Machine.objects.get(id=id)
	return render(request, 'details.html', {'machine_det' : machine})

def getNamaMesin(request):
	if request.method == "GET":
		AllMachine = Machine.objects.all()
		jsonMachine = { 'namaMesin' : [i.getNamaMesin() for i in AllMachine]}
		return JsonResponse(jsonMachine)
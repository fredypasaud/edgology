from django.db import models
import datetime
from django.core.validators import MaxValueValidator, MinValueValidator

# Create your models here.
def getTodayYear():
    t = datetime.date.today()
    return t.year

class Machine(models.Model):
    machine_name = models.CharField(max_length=200)
    machine_type = models.CharField(max_length=200)
    manufacturer = models.CharField(max_length=200)
    year_built = models.IntegerField(validators=[MinValueValidator(0), MaxValueValidator(getTodayYear())])
    location = models.CharField(max_length=200)
    status = models.IntegerField(validators=[MinValueValidator(0), MaxValueValidator(3)])

    def getNamaMesin(self):
        return self.machine_name
    def getTodayYear(self):
        return getTodayYear()
    def getTipeMesin(self):
        return self.machine_type
    def getManufaktur(self):
        return self.manufacturer
    def getTahun(self):
        return self.year_built
    def getLokasi(self):
        return self.location
    def getStatus(self):
        return self.status
    def statusSafe(self):
        self.status = 1
    def statusError(self):
        self.status = 3
    def statusRepair(self):
        self.status = 2
    def __str__(self):
        return str(self.machine_name)

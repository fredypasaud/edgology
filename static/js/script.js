function csrfSafeMethod(method) {
  // these HTTP methods do not require CSRF protection
  return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}

$(document).ready(function(){
$("#engineer-create-submit").click(
  function(event){
    event.preventDefault()
    // initialized field checking
    let checkstat = $("#id_namaEngineer").val();
    let checkstat2 = $("#id_divisiEngineer").val();
    let checkstat3 = $("#id_lokasiEngineer").val();
    let checkstat4 = $("#id_status").val();
    let csrftoken = jQuery("[name=csrfmiddlewaretoken]").val();
    $.ajaxSetup({
        beforeSend: function(xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    });
    $.ajax({
      url : '/engineer/create/',
      type : "POST",
      data : {
        // this data dictionary will be passed to django views
        namaEngineer : $("#id_namaEngineer").val(),
        divisiEngineer : $("#id_divisiEngineer").val(),
        lokasiEngineer : $("#id_lokasiEngineer").val(),
        status : $("#id_status").val(),
      },
      success : function(json) {
        //check if field is empty
      if( checkstat != '' && checkstat2 != '' && checkstat3 != '' && checkstat4 != ''){
        $("#alertsection").empty();
        $('#alertsection').append(
          "<div class='alert alert-success alert-dismissible fade show' role='alert'>"+
                "Sucess!" +
                "<button type='button' class='close' data-dismiss='alert' aria-label='Close'>" +
                  "<span aria-hidden='true'>&times;</span>" +
                "</button>" +
              "</div>"
        );
        $("form").trigger("reset");
      }
      else{
        $("form").trigger("reset");
        $("#alertsection").empty();
        $('#alertsection').append(
          "<div class='alert alert-danger alert-dismissible fade show' role='alert'>"+
                "Failed! Field cannot be empty" +
                "<button type='button' class='close' data-dismiss='alert' aria-label='Close'>" +
                  "<span aria-hidden='true'>&times;</span>" +
                "</button>" +
              "</div>"
        );
      }
    },
    })
})
$("#reportSubmit").click(
  function(event){
    event.preventDefault()
    let checkstat = $("#id_namaPembuatReport").val();
    let checkstat2 = $("#id_jenisReport").val();
    let checkstat3 = $("#id_detailReport").val();
    let csrftoken = jQuery("[name=csrfmiddlewaretoken]").val();
    $.ajaxSetup({
        beforeSend: function(xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    });
    $.ajax({
      type : "POST",
      data : {
        namaPembuatReport : $("#id_namaPembuatReport").val(),
        jenisReport : $("#id_jenisReport").val(),
        detailReport : $("#id_detailReport").val(),
      },
      success: function(json) {
        if(checkstat != '' && checkstat2 != '' && checkstat3 != ''){
          $("#alertsection").empty();
          $('#alertsection').append(
            "<div class='alert alert-success alert-dismissible fade show' role='alert'>"+
                  "Sucess!" +
                  "<button type='button' class='close' data-dismiss='alert' aria-label='Close'>" +
                    "<span aria-hidden='true'>&times;</span>" +
                  "</button>" +
                "</div>"
          );
          $("form").trigger("reset");
        }
        else{
          $("form").trigger("reset");
          $("#alertsection").empty();
          $('#alertsection').append(
            "<div class='alert alert-danger alert-dismissible fade show' role='alert'>"+
                  "Failed! Field cannot be empty" +
                  "<button type='button' class='close' data-dismiss='alert' aria-label='Close'>" +
                    "<span aria-hidden='true'>&times;</span>" +
                  "</button>" +
                "</div>"
          );
        }
      },
    })
  })
  $("#eng-input").keyup(
    function(){
      const url_parameter = {
             q: $(this).val()
         }
      $.ajax({
        type :"GET",
        dataType: "json",
        url : "/engineer/?q=" +$(this).val(),
        success : function(result){
          $('#replaceable-content').html(result['html_from_view'])
        }

      })
    }
  );

  $("#rep-input").keyup(
    function(){
      const url_parameter = {
             q: $(this).val()
         }
      $.ajax({
        type :"GET",
        dataType: "json",
        url : "/engineer/report/?q=" +$(this).val(),
        success : function(result){
          $('#replaceable-content').html(result['html_from_view'])
        }

      })
    }
  );
    // AHAHAHAHAHAHA AJAX-CEPTION
  $("#id_submit_mesin").click(
    function(event){
      event.preventDefault()
      $('#message_sign_mesin').remove();
      $.ajax({
        url: "getNamaMesin",
        method: "GET",
        dataType : "json",
        success : function(json){
          var namaForm = $('#id_machine_name').val();
          var where = json.namaMesin.includes(namaForm);
          console.log(json)
          if(where){
            alert("Nama mesin tidak boleh sama!")
          }else {
            $.ajax({
              url: "",
              method: "POST",
              dataType : "json",
              data: {
                machine_name : $('#id_machine_name').val(),
                machine_type : $('#id_machine_type').val(),
                manufacturer : $('#id_manufacturer').val(),
                year_built : $('#id_year_built').val(),
                location : $('#id_location').val()
              },
              success : function(response){
                console.log(response);
                $('#id_machine_name').val('');
                $('#id_machine_type').val('');
                $('#id_manufacturer').val('');
                $('#id_year_built').val('');
                $('#id_location').val('');
                var funNum = Math.floor(Math.random() * 3);
                if(funNum === 0){
                  $('.card-body').append('<p class="text-success" id="message_sign_mesin">Machine successfully added!</p>');
                }else if(funNum === 1){
                  $('.card-body').append('<p class="text-success" id="message_sign_mesin">Beep! Beep! Ready for work!</p>');
                }else if (funNum === 2){
                  $('.card-body').append('<p class="text-success" id="message_sign_mesin">New machine on the gang!</p>');
                }
              },
              error : function(jqXHR, textStatus, errorThrown){
                $('.card-body').append('<p class="text-danger" id="message_sign_mesin">Oh noes! Something went wrong!</p>');
              }
            })
          }
        },
        error : function(jqXHR, textStatus, errorThrown){
          $('.card-body').append('<p class="text-danger" id="message_sign_mesin">Oh noes! Something went wrong!</p>');
        }
      })
    })
    $("#id_submit_mesin").hover(function(){
      $("#id_submit_mesin").animate({
        height: '+=10px',
        width: '+=10px'
      });
      $("#id_submit_mesin").animate({
        height: '-=10px',
        width: '-=10px'
      })
    });

    $(window).load(function() {
      // Animate loader off screen
      $(".se-pre-con").fadeOut("slow");;
      });

})

# Generated by Django 2.2.6 on 2019-10-19 15:19

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('engineerpage', '0003_auto_20191019_1047'),
    ]

    operations = [
        migrations.AlterField(
            model_name='report',
            name='tanggalPembuatan',
            field=models.DateTimeField(auto_now_add=True),
        ),
    ]

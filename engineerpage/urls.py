from django.urls import include,path
from . import views

urlpatterns  = [
    path('', views.eng_search, name = 'overview'),
    path('create/', views.engineer_create_ajax, name = 'engineer_create'),
    path('<int:pk>', views.engineer_detail, name = 'engineer_detail'),
    path('report/', views.rep_search, name = 'report_overview'),
    path('report/create/', views.report_create_ajax, name = 'report_create'),
    path('report/<int:pk>', views.report_detail, name = 'report_detail'),
]

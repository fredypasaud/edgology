from django.test import TestCase, LiveServerTestCase
from .models import Engineer, Report
from . import views
from django.urls import reverse
from .forms import EngineerForms, ReportForms
from datetime import datetime, timezone

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.options import Options
import time


class EngineerTest(TestCase):

    def createEngineer(self, name = "Aryo", divisi = "LKDE", lokasi = "Jakarta Utara", status = "Available" ):
        return Engineer.objects.create(namaEngineer = name, divisiEngineer = divisi, lokasiEngineer = lokasi, status = status)

    def createReport(self):
        name = "Aryo"
        jenisReport = "Error Report"
        detailReport = "Ada error mesin yang terdeteksi"
        return Report.objects.create(namaPembuatReport = name, jenisReport = jenisReport, detailReport = detailReport)

    def testEng(self):
        c = self.createEngineer()
        self.assertTrue(isinstance(c, Engineer))
        self.assertEqual(c.__str__(), c.namaEngineer)
        url = reverse('overview')
        resp = self.client.get(url)
        self.assertEqual(resp.status_code, 200)


    def testReport(self):
        r = self.createReport()
        self.assertTrue(isinstance(r, Report))
        self.assertEqual(r.__str__(), r.namaPembuatReport)
        url = reverse('report_overview')
        resp = self.client.get(url)
        self.assertEqual(resp.status_code, 200)

    def test_eng_det(self):
        c = self.createEngineer()
        resp = self.client.get('/engineer/1')
        self.assertEqual(resp.status_code, 200)

    def test_rep_det(self):
        c = self.createReport()
        resp = self.client.get('/engineer/report/1')
        self.assertEqual(resp.status_code, 200)

    def test_rep_cre(self):
        form_data = {"namaPembuatReport" : "Aryo","jenisReport" : "TLE","detailReport" : "Ini adalah sebuah report",}
        form = ReportForms(data = form_data)
        self.assertTrue(form.is_valid())
        request = self.client.post('/engineer/report/create/', data = form_data)
        self.assertEqual(request.status_code, 200)

        response = self.client.get('/engineer/report/create/')
        self.assertEqual(response.status_code, 200)

    def test_eng_cre(self):
        form_data = {
        "namaEngineer" : "Aryo",
        "divisiEngineer" : "Test",
        "lokasiEngineer" : "Lokasi Test",
        "status" : "AVAIL TEST",

        }
        form = EngineerForms(data = form_data)
        self.assertTrue(form.is_valid())
        request = self.client.post('/engineer/create/', data = form_data)
        self.assertEqual(request.status_code, 200)

        response = self.client.get('/engineer/create/')
        self.assertEqual(response.status_code, 200)

class EngineerFunctionalTesting(LiveServerTestCase):

    def setUp(self):
        super(EngineerFunctionalTesting, self).setUp()
        firefox_options = Options()
        firefox_options.add_argument('--no-sandbox')
        firefox_options.add_argument('--headless')
        firefox_options.add_argument('--disable-gpu')
        self.browser = webdriver.Firefox(firefox_options = firefox_options)

    def tearDown(self):
        self.browser.quit()
        super(EngineerFunctionalTesting, self).tearDown()

    def test_engineer(self):
        self.browser.get('http://127.0.0.1:8000')
        # addeng = self.browser.find_element_by_id('addEng')
        username = self.browser.find_element_by_id('username')
        password = self.browser.find_element_by_id('password')
        loginButt = self.browser.find_element_by_id('submitLogin')
        username.send_keys('adminedgology')
        password.send_keys('adminedgo')
        loginButt.send_keys(Keys.RETURN)
        time.sleep(3)
        self.browser.get('http://127.0.0.1:8000/engineer/')
        time.sleep(3)
        self.browser.get('http://127.0.0.1:8000/engineer/create/')
        time.sleep(3)
        nama_eng = self.browser.find_element_by_id('id_namaEngineer')
        div_eng = self.browser.find_element_by_id('id_divisiEngineer')
        lok_eng = self.browser.find_element_by_id('id_lokasiEngineer')
        status_eng = self.browser.find_element_by_id('id_status')
        button_sub = self.browser.find_element_by_id("engineer-create-submit")
        nama_eng.send_keys("FuncTest")
        div_eng.send_keys("FuncTest")
        lok_eng.send_keys("FuncTest")
        status_eng.send_keys("FuncTest")
        button_sub.send_keys(Keys.RETURN)
        time.sleep(3)
        self.browser.get('http://127.0.0.1:8000/engineer/')
        time.sleep(3)
        self.assertInHTML('FuncTest', self.browser.page_source)
        time.sleep(3)

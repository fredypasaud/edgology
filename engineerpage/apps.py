from django.apps import AppConfig


class EngineerpageConfig(AppConfig):
    name = 'engineerpage'

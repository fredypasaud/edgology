from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse, JsonResponse
from .models import Engineer, Report
from .forms import EngineerForms, ReportForms
import json
from django.template.loader import render_to_string

def engineer_create_ajax(request):
    response_engineer = {}
    if request.method == 'POST':
        form = EngineerForms(request.POST)
        if form.is_valid():
            namaEngineer = request.POST.get('namaEngineer')
            divisiEngineer = request.POST.get('divisiEngineer')
            lokasiEngineer = request.POST.get('lokasiEngineer')
            status = request.POST.get('status')

            response_engineer['namaEngineer'] = namaEngineer
            response_engineer['divisiEngineer'] = divisiEngineer
            response_engineer['lokasiEngineer'] = lokasiEngineer
            response_engineer['status'] = status

            engineer = Engineer.objects.create(
                namaEngineer = namaEngineer,
                divisiEngineer = divisiEngineer,
                lokasiEngineer = lokasiEngineer,
                status = status,
                )
            return JsonResponse(response_engineer)
    else:
        form = EngineerForms()

    return render(request, 'engineer.html',{'form' : form,})

def report_create_ajax(request):
    response_report = {}
    if request.method == 'POST':
        formr = ReportForms(request.POST)
        if formr.is_valid():
            namaPembuatReport = request.POST.get('namaPembuatReport')
            jenisReport = request.POST.get('jenisReport')
            detailReport = request.POST.get('detailReport')

            response_report['namaPembuatReport'] = namaPembuatReport
            response_report['jenisReport'] = jenisReport
            response_report['detailReport'] = detailReport

            report = Report.objects.create(
                namaPembuatReport = namaPembuatReport,
                jenisReport = jenisReport,
                detailReport = detailReport,
            )
            return JsonResponse(response_report)
    else:
        formr = ReportForms()
    return render(request, 'report.html', {'formr' : formr,})

def engineer_detail(request,pk):
    engineer = Engineer.objects.get(pk=pk)
    return render(request, 'engineer_details.html', {'engineer' : engineer})

def report(request):
    reports = Report.objects.all()
    return render(request, 'report_list.html', {'reports' : reports})

def report_detail(request,pk):
    report = Report.objects.get(pk=pk)
    return render(request, 'report_detail.html', {'report' : report})

def eng_search(request):

    ctx = {}
    url_parameter = request.GET.get("q")

    if url_parameter:
        engineers = Engineer.objects.filter(namaEngineer__icontains= url_parameter)
    else:
        engineers = Engineer.objects.all()

    ctx["engineers"] = engineers

    if request.is_ajax():
        html = render_to_string(
            template_name="eng-results-partial.html",
            context={"engineers": engineers}
        )

        data_dict = {"html_from_view": html}

        return JsonResponse(data=data_dict, safe=False)

    return render(request, "engineer_list.html", context=ctx)

def rep_search(request):

    ctx_eng = {}
    url_parameter = request.GET.get("q")

    if url_parameter:
        reports = Report.objects.filter(namaPembuatReport__icontains= url_parameter)
    else:
        reports = Report.objects.all()

    ctx_eng["reports"] = reports

    if request.is_ajax():
        html = render_to_string(
            template_name="rep-results-partial.html",
            context={"reports": reports}
        )

        data_dict = {"html_from_view": html}

        return JsonResponse(data=data_dict, safe=False)

    return render(request, "report_list.html", context=ctx_eng)

from django.shortcuts import render,redirect
from django.http import HttpResponse
from django.contrib.auth import login, authenticate,logout
from django.contrib.auth.decorators import login_required

response = {}

@login_required(login_url='/login/')
def index(request):
	return render(request,'OK.html')

def user_logout(request):
    request.session.flush()
    logout(request)
    return redirect('login')

def user_login(request):
	if request.method == "POST":
		username = request.POST['username']
		password = request.POST['password']
		user = authenticate(request, username = username, password = password)
		if user is not None:
			login(request,user)
			request.session.set_expiry(0)
			return redirect('OK')
		else:
			return redirect('login')
	else:
		return render(request, 'login.html')

from django.test import TestCase, RequestFactory, Client, LiveServerTestCase
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from . import views
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.common.action_chains import ActionChains
import time

class UnitTestcase(TestCase):

    def setUp(self):
        self.factory = RequestFactory()
        self.user = User.objects.create_user(
        username='jacob', email='jacob@…', password='top_secret')

    def test_details(self):
        request = self.factory.get('/login/')
        request.user = self.user
        username = self.client.post(request.user.username)
        password = self.client.post(request.user.password)
        response = views.user_login(request)
        userlog = authenticate(request.user)
        # response = self.client.post(login(request, request.user))
        self.assertEqual(response.status_code, 200)

    def test_landing(self):
        response = self.client.get('')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'login.html')

class OurWebFunctionalTesting(LiveServerTestCase):

    def setUp(self):
        super(OurWebFunctionalTesting, self).setUp()
        firefox_options = Options()
        firefox_options.add_argument('--no-sandbox')
        firefox_options.add_argument('--headless')
        firefox_options.add_argument('--disable-gpu')
        self.browser = webdriver.Firefox(firefox_options = firefox_options)

    def tearDown(self):
        self.browser.quit()
        super(OurWebFunctionalTesting, self).tearDown()

    def test_login(self):
        self.browser.get('http://127.0.0.1:8000')
        username = self.browser.find_element_by_id('username')
        password = self.browser.find_element_by_id('password')
        loginButt = self.browser.find_element_by_id('submitLogin')
        username.send_keys('adminedgology')
        password.send_keys('adminedgo')
        loginButt.send_keys(Keys.RETURN)
        time.sleep(3)
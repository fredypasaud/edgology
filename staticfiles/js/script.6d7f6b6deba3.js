$(document).ready(function(){
  $("#engineer-create-submit").click(
    function(event){
      event.preventDefault()
      console.log("engineer is working")
      $.ajax({
        url : '/engineer/create/',
        type : "POST",
        data : {
          namaEngineer : $("#id_namaEngineer").val(),
          divisiEngineer : $("#id_divisiEngineer").val(),
          lokasiEngineer : $("#id_lokasiEngineer").val(),
          status : $("#id_status").val(),
        },

        success : function(json) {
        $('#id_namaEngineer').val('');
        $("#id_divisiEngineer").val('');
        $("#id_lokasiEngineer").val('');
        $("#id_status").val('');
        console.log(json); // log the returned json to the console
        console.log("success"); // another sanity check
        // Praise the sanity check
      },
      })
  })
  $("#id_submit_mesin").click(
    function(event){
      event.preventDefault()
      $('#message_sign_mesin').remove();
      $.ajax({
        url: "getNamaMesin",
        method: "GET",
        dataType : "json",
        success : function(json){
          var namaForm = $('#id_machine_name').val();
          var where = true;
          console.log(json)
          for(var i = 0; i < json.length; i++){
            var nama = json.namaMesin[i];
            console.log(nama)
            console.log("CIEEEEE")
            if(nama === namaForm){
              console.log("CIAAAA")
              alert("Nama mesin tidak boleh sama!");
              where = false;
              break;
            }
          }
          if(where){
            $.ajax({
              url: "",
              method: "POST",
              dataType : "json",
              data: {
                machine_name : $('#id_machine_name').val(),
                machine_type : $('#id_machine_type').val(),
                manufacturer : $('#id_manufacturer').val(),
                year_built : $('#id_year_built').val(),
                location : $('#id_location').val()
              },
              success : function(response){
                console.log(response);
                $('#id_machine_name').val('');
                $('#id_machine_type').val('');
                $('#id_manufacturer').val('');
                $('#id_year_built').val('');
                $('#id_location').val('');
                var funNum = Math.floor(Math.random() * 3);
                if(funNum === 0){
                  $('.card-body').append('<p class="text-success" id="message_sign_mesin">Machine successfully added!</p>');
                }else if(funNum === 1){
                  $('.card-body').append('<p class="text-success" id="message_sign_mesin">Beep! Beep! Ready for work!</p>');
                }else if (funNum === 2){
                  $('.card-body').append('<p class="text-success" id="message_sign_mesin">New machine on the gang!</p>');
                }
              },
              error : function(jqXHR, textStatus, errorThrown){
                $('.card-body').append('<p class="text-danger" id="message_sign_mesin">Oh noes! Something went wrong!</p>');
              }
            })
          }
        },
        error : function(jqXHR, textStatus, errorThrown){
          alert("KILL ME");
          $('.card-body').append('<p class="text-danger" id="message_sign_mesin">Oh noes! Something went wrong!</p>');
        }
      })
    })
    $("#id_submit_mesin").hover(function(){
      $("#id_submit_mesin").animate({
        height: '+=10px',
        width: '+=10px'
      });
      $("#id_submit_mesin").animate({
        height: '-=10px',
        width: '-=10px'
      })
    });
})

$(document).ready(function(){
  $("#engineer-create-submit").click(
    function(event){
      event.preventDefault()
      console.log("engineer is working")
      $.ajax({
        url : '/engineer/create/',
        type : "POST",
        data : {
          namaEngineer : $("#id_namaEngineer").val(),
          divisiEngineer : $("#id_divisiEngineer").val(),
          lokasiEngineer : $("#id_lokasiEngineer").val(),
          status : $("#id_status").val(),
        },

        success : function(json) {
        $('#id_namaEngineer').val('');
        $("#id_divisiEngineer").val('');
        $("#id_lokasiEngineer").val('');
        $("#id_status").val('');
        console.log(json); // log the returned json to the console
        console.log("success"); // another sanity check
      },
      })
  })
})

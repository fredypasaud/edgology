[![pipeline status](https://gitlab.com/fredypasaud/edgology/badges/master/pipeline.svg)](https://gitlab.com/fredypasaud/edgology/commits/master)

[![coverage report](https://gitlab.com/fredypasaud/edgology/badges/master/coverage.svg)](https://gitlab.com/fredypasaud/edgology/commits/master)

## Anggota Kelompok :
- Fredy Pasaud M.S 1806204915
- Carissa Syieva Qalbiena 1806186875
- Muhammad Tsaqif Al Bari 1806235731
- Alvia Dibby Shadqah - 1806191004

## Nama Aplikasi :
edgology
## Link Heroku App
[http://edgology-rebrand.herokuapp.com/]
## Background
Menganut tema Industri 4.0, dimana automation dan IoT sedang gencar gencarnya dicari. Kami merasa bahwa terkadang sebuah perusahaan yang memiliki banyak pabrik dapat kewalahan dalam memantau kinerja pabrik mereka. Oleh karena itu, pada tugas kali ini kami membuat sebuah website yang dapat memantau semua mesin yang dimiliki perusahaan tersebut (diceritakan saat men sign-up mesin otomatis mesin tersebut selalu terpantau oleh sistem). Dengan adanya website ini akan ada banyak perusahaan yang terbantu dalam mengawasi pabrik mereka dan menghemat uang yang sangat besar karena sebuah mesin dapat dipantau secara real-time sebelum ia rusak parah.
## Fitur :
Sebuah Dashboard yang akan menampilkan status dari semua pesin yang ada di perusahaan
Sebuah Overview yang menampilkan informasi sekilas tentang mesin mesin yang ada pada perusahaan tersebut.
Sebuah page Sign Up user, dan Sign in User
Sebuah page yang dapat mendaftarkan sebuah mesin untuk dimasukan kedalam suatu database.
Sebuah page details mesin yang menampilkan informasi mesin yang lengkap, beserta dengan history dari mesin tersebut (misal pernah rusak, atau error)
